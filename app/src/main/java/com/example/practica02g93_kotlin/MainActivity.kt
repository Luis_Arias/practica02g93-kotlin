package com.example.practica02g93_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var txtPeso: EditText
    private lateinit var txtAltura: EditText
    private lateinit var txtResultado: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRegresar = findViewById(R.id.btnRegresar)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        txtAltura = findViewById(R.id.txtAltura)
        txtPeso = findViewById(R.id.txtPeso)
        txtResultado = findViewById(R.id.txtResultado)

        btnLimpiar.setOnClickListener {
            txtResultado.setText("")
            txtPeso.setText("")
            txtAltura.setText("")
            txtAltura.requestFocus()
            txtPeso.requestFocus()
        }

        btnCalcular.setOnClickListener {
            if (txtAltura.text.toString().isEmpty() || txtPeso.text.toString().isEmpty()) {
                Toast.makeText(this@MainActivity, "Captura en todos los campos", Toast.LENGTH_SHORT).show()
            } else {
                val altura = txtAltura.text.toString().toFloat()
                val peso = txtPeso.text.toString().toFloat()
                val imc = peso / (altura * altura)
                val resultado = String.format("%.2f", imc)
                txtResultado.setText(resultado)
            }
        }

        btnRegresar.setOnClickListener { finish() }
    }
}